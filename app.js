const fs = require('fs');
const randChoice = (obj) => { return Math.floor(Math.random() * obj.length); }

const data = {
  genders: ['M', 'F'],
  names: {
    'M': ['William', 'James', 'Michael', 'Alexander', 'Daniel', 'Christopher', 'Matthew', 'David', 'Joseph', 'Tyler'],
    'F': ['Sophia', 'Olivia', 'Emma', 'Ava', 'Isabella', 'Mia', 'Charlotte', 'Amelia', 'Abigail', 'Emily']
  },
  lastNames: ['Smith', 'Johnson', 'Brown', 'Taylor', 'Miller', 'Wilson', 'Jackson', 'Davis', 'Robinson', 'Wright'],
  emailDomains: ['gmail.com', 'yahoo.com', 'proton.me', 'outlook.com', 'hotmail.com', 'yandex.com', 'aol.com', 'hush.com']
};

const peoples = [];

for(let i = 0; i < 20; i++) {
  const gender = data.genders[randChoice(data.genders)];
  const namesArr = data.names[gender];
  const name = namesArr[randChoice(namesArr)];
  const lastName = data.lastNames[randChoice(data.lastNames)];
  const email = `${name}.${lastName}@${data.emailDomains[randChoice(data.emailDomains)]}`.toLowerCase();
  
  peoples.push({ gender, name, lastName, email });
}

fs.writeFile('people.json', JSON.stringify(peoples), (err) => {
  if(err) throw new Error("Something went wrong");
  console.log("File has been successfully generated! Check people.json");
});